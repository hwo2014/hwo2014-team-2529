package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import java.lang.Math;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonNull;
import com.google.gson.JsonPrimitive;

public class Main {
	public static void main(String... args) throws IOException {
		Locale.setDefault(Locale.ENGLISH);
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer =
				new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

		final BufferedReader reader =
				new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

		new Main(reader, writer, new Join(botName, botKey));
	}

	//final Gson gson = new Gson();
	final JsonParser jsonParser = new JsonParser();
	private PrintWriter writer;

	public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
		this.writer = writer;
		String line = null;
		
		boolean sendTicks = true;
		
		RaceStatus raceStatus = new RaceStatus();
		Track track = new Track();
		RaceType race = new RaceType();
		Car[] cars = new Car[0]; //liittyy varsinaiseen taulukon gameInitin yhteydessä
		HashMap<String, Integer> carIdsByColor = new HashMap<String, Integer>();
		String myCarColor = null;
		int myCarId = -1;
		ArrayList<Double> throttles = new ArrayList<Double>();
		double currentThrottle = 0.0;
		Turbo turbo = new Turbo();
		
		Model model = new Model();
		
		ArrayList<String> newCrashers = new ArrayList<String>();
		ArrayList<String> newSpawners = new ArrayList<String>();
		ArrayList<String> newDnfs = new ArrayList<String>();
		ArrayList<String> newFinishers = new ArrayList<String>();
		
		//Ai ai = new ConstantThrottleAi(1.0);
// kulma 60 crashaa?		
		/*PreScheduledAi psAi = new PreScheduledAi();
		psAi.add(new Throttle(1.0), 689);
		psAi.add(new Throttle(0.4), 20);
		Ai ai = psAi;/**/
		//Ai ai = new ConstantVelocityAi(5.9329588);
		Ai ai = new HopefullyUsefulPartOfSmartAi(5.9329588);
		
		send(join);
		
		while((line = reader.readLine()) != null) {
			final JsonObject msg = jsonParser.parse(line).getAsJsonObject();
			//System.out.println("Received: " + line);
			final String msgType = msg.get("msgType").getAsString();			
			if (msgType.equals("carPositions")) {
				raceStatus.addCarPositions(msg, carIdsByColor, track);
				if (msg.has("gameTick")) {
					int tick = msg.get("gameTick").getAsInt();
					if (!newCrashers.isEmpty()) {
						raceStatus.addCrashers(newCrashers, tick, carIdsByColor);
						newCrashers.clear();
					}
					if (!newSpawners.isEmpty()) {
						raceStatus.addSpawners(newSpawners, tick, carIdsByColor);
						newSpawners.clear();
					}
					if (!newDnfs.isEmpty()) {
						raceStatus.addDnfs(newDnfs, tick, carIdsByColor);
						newDnfs.clear();
					}
					if (!newFinishers.isEmpty()) {
						raceStatus.addFinishers(newFinishers, tick, carIdsByColor);
						newFinishers.clear();
					}
					raceStatus.printStates(tick, cars);
					if (tick == 4)
						model.computeForwardConstans(myCarId, raceStatus, throttles, 1);
					if (tick == 1)
						track.computeMaxLineVelocities(track.trackPieces);
					ai.getDistanceToNextCurve(tick, track, raceStatus, myCarId);
					//ai.test();
					SendMsg action = ai.getAction(tick, raceStatus, myCarId, model);
					//ai.test();
					if (sendTicks)
						action.setTick(tick);
					if (action instanceof Throttle)
						currentThrottle = ((Throttle)action).value;
					send(action);
					while (throttles.size() <= tick)
						throttles.add(null);
					throttles.set(tick, currentThrottle);
				}
			}
			else if (msgType.equals("crash")) {
				String crasherCol = msg.getAsJsonObject("data").get("color").getAsString();
				int tick = msg.get("gameTick").getAsInt();
				System.out.format("'%s' crashed at tick: %d \n", crasherCol, tick);
				newCrashers.add(crasherCol);
			}
			else if (msgType.equals("spawn")) {
				String spawnerCol = msg.getAsJsonObject("data").get("color").getAsString();
				int tick = msg.get("gameTick").getAsInt();
				newSpawners.add(spawnerCol);
				System.out.format("'%s' spawned at tick: %d \n", spawnerCol, tick);
			}
			else if (msgType.equals("lapFinished")) {
				// TODO
				//raceState.lapFinished(msg);
				System.out.format("LAP FINISHED, tick = %d\n", msg.get("gameTick").getAsInt());
			}
			else if (msgType.equals("dnf")) {
				String dnfCol = msg.getAsJsonObject("data").getAsJsonObject("car").get("color").getAsString();
				int tick = msg.get("gameTick").getAsInt();
				newDnfs.add(dnfCol);
				System.out.format("'%s' dnf'ed at tick: %d \n", dnfCol, tick);
			}
			else if (msgType.equals("finish")) {
				String finisherCol = msg.getAsJsonObject("data").get("color").getAsString();
				int tick = msg.get("gameTick").getAsInt();
				newFinishers.add(finisherCol);
				System.out.format("'%s' finished at tick: %d \n", finisherCol, tick);
			}
			else if (msgType.equals("join")) {
				System.out.println("Joined");
				// TODO
			}
			else if (msgType.equals("yourCar")) {
				myCarColor = msg.getAsJsonObject("data").get("color").getAsString();
			}
			else if (msgType.equals("gameInit")) {
				track.addTrack(msg);
				track.print();
				cars = addCars(msg);
				for (Car car : cars)
					carIdsByColor.put(car.color, car.id);
				myCarId = carIdsByColor.get(myCarColor);
				printCars(cars);
				race.addRaceType(msg);
				race.print();
				System.out.println("Race init");
			}
			else if (msgType.equals("gameStart")) {
				System.out.println("Race start");
					int tick = 0;
					SendMsg action = ai.getAction(tick, raceStatus, myCarId, model);
					if (sendTicks)
						action.setTick(tick);
					if (action instanceof Throttle)
						currentThrottle = ((Throttle)action).value;
					send(action);
					throttles.add(currentThrottle);
			}
			else if (msgType.equals("gameEnd")) {
				System.out.println("Race finish");
				// TODO
			}
			else if (msgType.equals("tournamentEnd")) {
				System.out.println("Race end");
				// TODO
			}
			else if (msgType.equals("turboAvailable")) {
				System.out.println("Turbo is now available");
				turbo.addTurbo(msg);
			}
			else {
				System.out.println("Unknown message type: " + line);
				//send(new Ping());
			}
		}
	}
	
	public Car[] addCars(JsonObject msg) {
		JsonArray newCars = msg.getAsJsonObject("data").getAsJsonObject("race").getAsJsonArray("cars");
		Car[] cars = new Car[newCars.size()];
		for (int i = 0; i < newCars.size(); i++) {
			if (!newCars.get(i).isJsonObject()) {
				continue;
			}
			Car car = new Car();
			JsonObject newCar = newCars.get(i).getAsJsonObject();
			car.id = i;
			car.name = newCar.getAsJsonObject("id").get("name").getAsString();
			car.color = newCar.getAsJsonObject("id").get("color").getAsString();
			car.length = newCar.getAsJsonObject("dimensions").get("length").getAsDouble();
			car.width = newCar.getAsJsonObject("dimensions").get("width").getAsDouble();
			car.gfPos = newCar.getAsJsonObject("dimensions").get("guideFlagPosition").getAsDouble();
			cars[i] = car;
		}
		return cars;
	}
		
	private void printCars(Car[] cars) {
/*		for (int i = 0; i < cars.length; i++) {
			System.out.format("car[%d]: name=%s, color=%s\n", i, cars[i].name, cars[i].color);
			System.out.format("         length=%g, width=%g, gfPos=%g\n", cars[i].length, cars[i].width, cars[i].gfPos);
		
		}
		*/
		for (int i = 0; i < cars.length; i++) {
			cars[i].print();
		}
	}
	

	private void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
		System.out.println("Sent: " + msg.toJson());
	}
}


abstract class Ai {
	public abstract SendMsg getAction(int tick, RaceStatus raceStatus, int myCarId, Model model);
	public abstract double getDistanceToNextCurve(int tick, Track track, RaceStatus raceStatus, int myCarId);
}
/*
class ConstantThrottleAi extends Ai {
	private double throttle_;
	public ConstantThrottleAi(double throttle) {
		throttle_ = throttle;
	}
	public SendMsg getAction(int tick, RaceStatus raceStatus, int myCarId, Model model) {
		return new Throttle(throttle_);
	}
}

class ConstantVelocityAi extends Ai {
	private double targetVelocity_;
	public ConstantVelocityAi(double targetVelocity) {
		targetVelocity_ = targetVelocity;
	}
	public SendMsg getAction(int tick, RaceStatus raceStatus, int myCarId, Model model) {
		Double currentVelocity = raceStatus.raceStates.get(tick).carStates[myCarId].velocity;
		if (currentVelocity == null)
			return new Throttle(1.0);
		Double throttle = model.getTargetVelocityThrottle(currentVelocity, targetVelocity_);
		if (throttle == null)
			return new Throttle(1.0);
		if (throttle < 0.0)
			throttle = 0.0;
		else if (throttle > 1.0)
			throttle = 1.0;
		return new Throttle(throttle);
	}
}

class PreScheduledAi extends Ai {
	private ArrayList<Integer> durations = new ArrayList<Integer>();
	private ArrayList<SendMsg> actions = new ArrayList<SendMsg>();
	private int currentIndex = 0;
	private int currentDuration = 0;
	
	void add(SendMsg action, int duration) {
		if (duration < 1)
			throw new Error("Too short duration, should be at least 1");
		actions.add(action);
		durations.add(duration);
	}
	public SendMsg getAction(int tick, RaceStatus raceStatus, int myCarId, Model model) {
		SendMsg action = null;
		if (currentIndex <= actions.size())
			action = actions.get(currentIndex);
		else
			action = new Ping();
		
		++currentDuration;
		if (currentDuration >= durations.get(currentIndex)) {
			++currentIndex;
			currentDuration = 0;
		}
		return action;
	}
}
*/

class HopefullyUsefulPartOfSmartAi extends Ai {
	private double targetVelocity_;
	private double distanceOfNextEnemyInMyLine_;
	private double distanceOfPrevEnemyInMyLine_;
	private double distanceOfNextCurve_;
	private double velocityOfNextEnemyInMyLine_;
	private double velocityOfPrevEnemyInMyLine_;
	private double velocityOfNextCurve_;
	
	public HopefullyUsefulPartOfSmartAi(double targetVelocity) {
		targetVelocity_ = targetVelocity;
	}
	public SendMsg getAction(int tick, RaceStatus raceStatus, int myCarId, Model model) {
		Double currentVelocity = raceStatus.raceStates.get(tick).carStates[myCarId].velocity;
		if (currentVelocity == null)
			return new Throttle(1.0);
		Double throttle = model.getTargetVelocityThrottle(currentVelocity, targetVelocity_);
		if (throttle == null)
			return new Throttle(1.0);
		if (throttle < 0.0)
			throttle = 0.0;
		else if (throttle > 1.0)
			throttle = 1.0;
		return new Throttle(throttle);
	}
	
	public void test(){
		System.out.println("hei");
	}
	//jos kaarre jyrkkenee loppua kohti, tämä on ehkä liian myöhässä
	public double getDistanceToNextCurve(int tick, Track track, RaceStatus raceStatus, int myCarId) {
		int myCarPieceIndex = raceStatus.raceStates.get(tick).carStates[myCarId].pieceIndex;
		double myCarInPieceDist = raceStatus.raceStates.get(tick).carStates[myCarId].inPieceDist;
		int nextCurveIndex = -1;
		double nextCurveDist = 0;
		RaceStatus.CarState prevState = raceStatus.raceStates.get(tick-1).carStates[myCarId];
		
		nextCurveDist = track.getLanePieceLength(myCarPieceIndex, prevState.startLane, prevState.endLane) - myCarInPieceDist;
		for (int i = myCarPieceIndex + 1; i < track.trackPieces.length; i++) {
			if (track.trackPieces[i].type == TrackPiece.Type.Bend) {
				nextCurveIndex = i;
				break;
			}
			nextCurveDist += track.getLanePieceLength(i, prevState.startLane, prevState.endLane);
		}
		if (nextCurveIndex == -1) {
			for (int i = 0; i < myCarPieceIndex + 1; i++) {
				if (track.trackPieces[i].type == TrackPiece.Type.Bend) {
					nextCurveIndex = i;
					break;
				}
				nextCurveDist += track.getLanePieceLength(i, prevState.startLane, prevState.endLane);
			}
		}
		System.out.format("nextCurveDist = %g\n", nextCurveDist);
		return nextCurveDist;
	}
	
	
}


class Model {
	public Double acceleration = null;
	public Double velocityDamping = null;
	
	public Double slipAcceleration = null;
	public Double slipResistance = null;
	public Double slipVelocityDamping = null;
	public Double maxSlipAngle = 60.0;
	
	void computeForwardConstans(int myCarId, RaceStatus raceStatus, ArrayList<Double> throttle, int firstTick) {
		//acceleration = 0.2;
		//velocityDamping = 0.98;
		double v0 = raceStatus.raceStates.get(firstTick).carStates[myCarId].velocity;
		double v1 = raceStatus.raceStates.get(firstTick+1).carStates[myCarId].velocity;
		double v2 = raceStatus.raceStates.get(firstTick+2).carStates[myCarId].velocity;
		double t0 = throttle.get(firstTick);
		double t1 = throttle.get(firstTick+1);
		acceleration = (v2 * v0 - v1 * v1) / (t1 * v0 - t0 * v1);
		velocityDamping = (v1 * t1 - t0 * v2) / (t1 * v0 - t0 * v1);
		System.out.println("acceleration = " + acceleration);
		System.out.println("velocityDamping = " + velocityDamping);
	}

	void computeSlipConstans(int myCarId, RaceStatus raceStatus, ArrayList<Double> throttle, int firstTick) {
		// TODO
		slipResistance = 0.00125;
		slipVelocityDamping = 0.9;
		//slipAcceleration = ;
	}
	
	Double getTargetVelocityThrottle(double currentVelocity, double targetVelocity) {
		if (acceleration != null && velocityDamping != null)
			return (targetVelocity - velocityDamping * currentVelocity) / acceleration;
		else
			return null;
	}
}




class RaceStatus {
	public enum State {
			OnLine, Crashed, Dnf, Finished
		}
	public class CarState {
		public int lap;
		public int pieceIndex;
		public double inPieceDist;
		public int startLane;
		public int endLane;
		public double angle;
		public State state = State.OnLine;
		public int crashTicks;
		public Double velocity = null;
		
		void computeVelocity(CarState prevState, Track track) {
			if (pieceIndex == prevState.pieceIndex) {
				velocity = inPieceDist - prevState.inPieceDist;
			}
			else {
				double prevLength = track.getLanePieceLength(prevState.pieceIndex,
						prevState.startLane, prevState.endLane);
				velocity  = inPieceDist + prevLength - prevState.inPieceDist;
				// FIXME: voiko ylittää monta palaa yhdellä tickillä?
			}
		}
	}
	public class RaceState {
		CarState[] carStates;
	}
	ArrayList<RaceState> raceStates = new ArrayList<RaceState>();
	
	public void addCarPositions(JsonObject msg, HashMap<String, Integer> carIdsByColor, Track track) {
		int tick = msg.has("gameTick") ? msg.get("gameTick").getAsInt() : 0;
		JsonArray data = msg.getAsJsonArray("data");
		RaceState raceState = new RaceState();
		CarState[] carStates = new CarState[carIdsByColor.size()];
		for (JsonElement elem : data) {
			JsonObject carPos = elem.getAsJsonObject();
			//String name = carPos.getAsJsonObject("id").get("name").getAsString();
			String color = carPos.getAsJsonObject("id").get("color").getAsString();
			if (!carIdsByColor.containsKey(color)) {
				// tuntematon kilpailija
				continue;
			}
			int id = carIdsByColor.get(color);
			CarState carState = new CarState();
			carState.angle = carPos.get("angle").getAsDouble();
			JsonObject piecePos = carPos.getAsJsonObject("piecePosition");
			carState.lap = piecePos.get("lap").getAsInt();
			carState.pieceIndex = piecePos.get("pieceIndex").getAsInt();
			carState.inPieceDist = piecePos.get("inPieceDistance").getAsDouble();
			carState.startLane = piecePos.getAsJsonObject("lane").get("startLaneIndex").getAsInt();
			carState.endLane = piecePos.getAsJsonObject("lane").get("endLaneIndex").getAsInt();
			// kopsaa staten ja päivittää crashTickin
			if (tick > 0) {
				carState.state = raceStates.get(tick-1).carStates[id].state;
				if (carState.state == State.Crashed)
					carState.crashTicks = raceStates.get(tick-1).carStates[id].crashTicks + 1;
			}
			
			carStates[id] = carState;
			System.out.format("'%s':lap=%d, piece=%d, dist=%.17f, angle=%.17f, gameTick=%d\n", color, carState.lap, carState.pieceIndex, carState.inPieceDist, carState.angle, tick);
		}
		raceState.carStates = carStates;
		while (raceStates.size() <= tick)
			raceStates.add(null);
		/*
		if (raceStates(tick) != null) {
			State[] statesOfCars = new State[data.size()];
			for (int i = 0; i < data.size(); i++)
				statesOfCars[i] = raceStates(tick).carStates[i].state
			raceStates.set(tick, raceState);
			for (int i = 0; i < data.size(); i++)
				raceStates(tick).carStates[i].state = statesOfCars[i];
		}
		//autojen tilat säilyy samana, jos ei oo tullu päivitystä
		else {
			raceStates.set(tick, raceState);
			if (tick > 0)
				for (int i = 0; i < data.size(); i++) {
					raceStates(tick).carStates[i].state = raceStates(tick-1).carStates[i].state;
					raceStates(tick).carStates[i].crashTime++;
				}
		}
		*/
		raceStates.set(tick, raceState);
		
		// lasketaan nopeudet
		if (tick > 0 && raceStates.get(tick-1) != null) {
			for (int i = 0; i < carIdsByColor.size(); ++i) {
				carStates[i].computeVelocity(raceStates.get(tick-1).carStates[i], track);
				System.out.format("velocity=%.17f\n", carStates[i].velocity);
			}
		}
	}

	public void addCrashers(ArrayList<String> newCrashers, int tick, HashMap<String, Integer> carIdsByColor) {
		while (raceStates.get(tick) == null && tick > -1)
			tick--;
		if (tick < 0) {
			System.out.println("addCrasher: raceStates = null? -> did nothing");
			return;
		}
		for (int i = 0; i < newCrashers.size(); i++) {
			int id = carIdsByColor.get(newCrashers.get(i));
			raceStates.get(tick).carStates[id].state = State.Crashed;
			raceStates.get(tick).carStates[id].crashTicks = 0;
		}
	}
	
	public void addSpawners(ArrayList<String> newSpawners, int tick, HashMap<String, Integer> carIdsByColor) {
		while (raceStates.get(tick) == null && tick > -1)
			tick--;
		if (tick < 0) {
			System.out.println("addSpawner: raceStates = null? -> did nothing");
			return;
		}
		for (int i = 0; i < newSpawners.size(); i++) {
			int id = carIdsByColor.get(newSpawners.get(i));
			raceStates.get(tick).carStates[id].state = State.OnLine;
			// lienee turha, mutta varmuuden vuoksi.
			raceStates.get(tick).carStates[id].crashTicks = 0;
		}
	}
	
	public void addDnfs(ArrayList<String> newDnfs, int tick, HashMap<String, Integer> carIdsByColor) {
		while (raceStates.get(tick) == null && tick > -1)
			tick--;
		if (tick < 0) {
			System.out.println("addDnf: raceStates = null? -> did nothing");
			return;
		}
		for (int i = 0; i < newDnfs.size(); i++) {
			int id = carIdsByColor.get(newDnfs.get(i));
			raceStates.get(tick).carStates[id].state = State.Dnf;
		}
	}
	
	public void addFinishers(ArrayList<String> newFinishers, int tick, HashMap<String, Integer> carIdsByColor) {
		while (raceStates.get(tick) == null && tick > -1)
			tick--;
		if (tick < 0) {
			System.out.println("addFinishers: raceStates = null? -> did nothing");
			return;
		}
		for (int i = 0; i < newFinishers.size(); i++) {
			int id = carIdsByColor.get(newFinishers.get(i));
			raceStates.get(tick).carStates[id].state = State.Finished;
		}
	}
	
	
	public void printStates(int tick, Car[] cars) {
		if (raceStates.get(tick) == null) {
			System.out.format("Cars don't seem to have state in tick %d.", tick);
			return;
		}
		for (int i = 0; i < cars.length; i++) {
			System.out.format("'%ss' state = ", cars[i].color);
			System.out.print(raceStates.get(tick).carStates[i].state);
			System.out.format(", crashTicks = %s\n", raceStates.get(tick).carStates[0].crashTicks);
		}
	}
	


/*	
	public void addCrasher(JsonObject msg) {
		int tick = msg.has("gameTick") ? msg.get("gameTick").getAsInt() : 0;
		RaceState raceState = new RaceState();
		CarState[] carStates = new CarState[data.size()];
		for (int i = 0; i < )
		while (raceStates.size() <= tick)
			raceStates.add(null);
		raceStates.set(tick, raceState);
	}
*/	
	public void lapFinished(JsonObject msg) {
		
	}
}

class TrackPiece {
	public enum Type {
		Straight, Bend
	}
	//oletuksena palassa ei ole kytkintä
	public Type type;
	public boolean sw = false;
	public int length;
	public int radius;
	public double angle;
	public double[] maxVelocity; //eri urille omat
}
/*
class Straight extends TrackPiece {
	public int length;
}

class Bend extends TrackPiece {
	public int radius;
	public double angle
}

*/

/*
Radan tiedot
*/
class Track {
	public String id;
	public String name;
	public TrackPiece[] trackPieces;
	public double[] laneDists;
	public double startX;
	public double startY;
	public double startAngle;
	public int nLanes;
	
	public void addTrack(JsonObject msg) {
		JsonObject data = msg.getAsJsonObject("data");
		JsonObject race = data.getAsJsonObject("race");
		JsonObject atrack = race.getAsJsonObject("track");
		// id ja nimi
		id = atrack.get("id").getAsString();
		name = atrack.get("name").getAsString();
		//palojen tiedot
		JsonArray pieces = atrack.getAsJsonArray("pieces");
		trackPieces = new TrackPiece[pieces.size()];
		for (int i = 0; i < pieces.size(); ++i) {
			if (!pieces.get(i).isJsonObject()) {
				// TODO: pitäis varmaan heittää poikkeus tms?
				continue;
			}
			JsonObject piece = pieces.get(i).getAsJsonObject();
			TrackPiece trackPiece = new TrackPiece();
			if (piece.has("length")) {
				trackPiece.type = TrackPiece.Type.Straight;
				trackPiece.length = piece.get("length").getAsInt();
			}
			if (piece.has("switch")) {
				trackPiece.sw = piece.get("switch").getAsBoolean();
			}
			if (piece.has("radius")) {
				trackPiece.type = TrackPiece.Type.Bend;
				trackPiece.radius = piece.get("radius").getAsInt();
			}
			if (piece.has("angle")) {
				trackPiece.angle = piece.get("angle").getAsDouble();
			}
			trackPieces[i] = trackPiece;
		}
		// urat
		JsonArray jsonLanes = atrack.getAsJsonArray("lanes");
		nLanes = jsonLanes.size();
		laneDists = new double[jsonLanes.size()];
		for (int i = 0; i < jsonLanes.size(); ++i) {
			if (!jsonLanes.get(i).isJsonObject()) {
				continue;
			}
			JsonObject lane = jsonLanes.get(i).getAsJsonObject();
			laneDists[i] = lane.get("distanceFromCenter").getAsDouble();
		}
		for (int i = 0; i < trackPieces.length; ++i) {
			trackPieces[i].maxVelocity = new double[nLanes];
		}
		
		//startingpos
		JsonObject pos = atrack.getAsJsonObject("startingPoint");
		startX = pos.getAsJsonObject("position").get("x").getAsDouble();
		startY = pos.getAsJsonObject("position").get("y").getAsDouble();
		startAngle = pos.get("angle").getAsDouble();
	}
	
	double getLanePieceLength(int pieceIndex, int startLane, int endLane) {
		TrackPiece piece = trackPieces[pieceIndex];
		switch (piece.type) {
			case Straight:
				if (startLane == endLane) {
					return piece.length;
				}
				else {
					// FIXME: pitäis selvittää, miten oikeasti menee
					double laneDiff = Math.abs(laneDists[startLane] - laneDists[endLane]);
					return Math.sqrt(piece.length * piece.length + laneDiff * laneDiff);
				}
			case Bend:
				if (startLane == endLane) {
					double s = Math.signum(piece.angle);
					double angle = Math.abs(piece.angle);
					double radius = piece.radius - s * laneDists[startLane];
					return radius * angle / 180 * Math.PI;
				}
				else {
					// FIXME: pitäis selvittää, miten oikeasti menee
					return 1.0;
				}
			default:
				assert(false);
		}
		return Double.NaN;
	}
	
	public void computeMaxLineVelocities(TrackPiece[] trackPieces) {
		int j = 0;
		for (int i = 0; i < trackPieces.length; i++) {
			if (trackPieces[i].type == TrackPiece.Type.Straight)
				for (j = 0; j < nLanes; j++)
					trackPieces[i].maxVelocity[j] = -1.0;
			else {
				//TODO
				for (j = 0; j < nLanes; j++)
					trackPieces[i].maxVelocity[j] = 5.9329588;
			}
		}
		//tulostus
		for (int i = 0; i < trackPieces.length; i++) {
			for (j = 0; j < nLanes; j++)
				System.out.format("Piece %d, line %d maxVel = %g\n", i, j, trackPieces[i].maxVelocity[j]);
		}
	}
	
	public void print() {
		int i = 0;
		for (i = 0; i < laneDists.length; i++) {
			System.out.format("lane %d: %g \n", i , laneDists[i]);
		}
		//startx, -y, -angle
		System.out.format("startX=%g, startY=%g, startAngle=%g\n", startX, startY, startAngle);
		//id, name
		System.out.format("id=%s, name=%s, palojen tiedot:\n", id, name);
		for (i = 0; i < trackPieces.length; i++) {
			System.out.format("%d: length = %d, switch = %b, radius = %d, angle = %g, \n", i, trackPieces[i].length, trackPieces[i].sw, trackPieces[i].radius, trackPieces[i].angle);
		}
	}
}

/*
Kaara
*/
class Car {
	public int id;
	public String name;
	public String color;
	public double length;
	public double width;
	public double gfPos;
	
	public void print() {
		System.out.format("car[%d]: name=%s, color=%s\n", id, name, color);
		System.out.format("         length=%g, width=%g, gfPos=%g\n", length, width, gfPos);
	}
	
}


class RaceType {
	public enum Type {
		QuickRace, Qualifying, Race
	}
	public Type type;
	int laps;
	int maxLapTimeMs;
	int durationMs;

	public void addRaceType(JsonObject msg) {
		JsonObject session = msg.getAsJsonObject("data").getAsJsonObject("race").getAsJsonObject("raceSession");
		if (session.has("quickRace")) {
			Boolean quickRace = session.get("quickRace").getAsBoolean();
			laps = session.get("laps").getAsInt();
			maxLapTimeMs = session.get("maxLapTimeMs").getAsInt();
			if (quickRace)
				type = Type.QuickRace;
			else
				type = Type.Race;
		}
		else {
			durationMs = session.get("durationMs").getAsInt();
			type = Type.Qualifying;
		}
	}
	
	public void print() {
		System.out.format("type: %s, ", type);
		if (type == Type.Qualifying)
			System.out.format("durationMS=%d\n", durationMs);
		else
			System.out.format("laps=%s, maxLapTimeMs=%s\n", laps, maxLapTimeMs);
	}
	
}


class Turbo {
	double durMillis;
	int durTicks;
	double factor;
	boolean available;

	// mikä oikeaoppisuus!
	public Turbo() {
		durMillis = 0.0;
		durTicks = 0;
		factor = 0;
		available = false;
	}
	
	public void addTurbo(JsonObject msg) {
		JsonObject data = msg.getAsJsonObject("data");
		durMillis = data.get("turboDurationMilliseconds").getAsDouble();
		durTicks = data.get("turboDurationTicks").getAsInt();
		factor = data.get("turboFactor").getAsDouble();
		available = true;
	}
	
	public void useTubro() {
		System.out.println("use Lightning Bolt!");
		available = false;
	}

}


abstract class SendMsg {
	protected Integer tick = null;
	
	public String toJson() {
		//return new Gson().toJson(new MsgWrapper(this));
		JsonObject msg = new JsonObject();
		msg.addProperty("msgType", msgType());
		msg.add("data", msgData());
		if (tick != null)
			msg.addProperty("gameTick", tick);
		return new Gson().toJson(msg);
	}
	
	public void setTick(Integer tick) {
		this.tick = tick;
	}

	protected abstract JsonElement msgData();

	protected abstract String msgType();
}

/*class MsgWrapper {
	public final String msgType;
	public final Object data;

	MsgWrapper(final String msgType, final Object data) {
		this.msgType = msgType;
		this.data = data;
	}

	public MsgWrapper(final SendMsg sendMsg) {
		this(sendMsg.msgType(), sendMsg.msgData());
	}
}*/

class Join extends SendMsg {
	public final String name;
	public final String key;

	Join(final String name, final String key) {
		this.name = name;
		this.key = key;
	}
	
	@Override
	protected String msgType() {
		return "join";
	}

	@Override
	protected JsonElement msgData() {
		JsonObject data = new JsonObject();
		data.addProperty("name", name);
		data.addProperty("key", key);
		return data;
	}
}

class Ping extends SendMsg {
	@Override
	protected String msgType() {
		return "ping";
	}
	
	@Override
	protected JsonElement msgData() {
		return JsonNull.INSTANCE;
	}
}

class Throttle extends SendMsg {
	public double value;

	public Throttle(double value, Integer tick) {
		this.value = value;
		this.tick = tick;
	}

	public Throttle(double value) {
		this.value = value;
		}
	
	@Override
	protected String msgType() {
		return "throttle";
	}

	@Override
	protected JsonElement msgData() {
		return new JsonPrimitive(value);
	}
}
